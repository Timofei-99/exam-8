import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import QuoteEdit from "./Components/QuoteEdit/QuoteEdit";
import Header from "./Components/UI/Header/Header";
import AddQuote from "./Container/AddQuote/AddQuote";
import QuotesList from "./Container/QuotesList/QuotesList";

const App = () => (
    <BrowserRouter>
      <Header />
      <div className="container">
        <Switch>
          <Route path="/" exact component={QuotesList} />
          <Route path="/quotes/:id" component={QuotesList} />
          <Route path="/add-quote" component={AddQuote} />
          <Route path="/edit/:id" component={QuoteEdit} />
        </Switch>
      </div>
    </BrowserRouter>
);

export default App;
