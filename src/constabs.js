export const CATEGORY = [
    { title: "Star Wars", id: "star-wars" },
    { title: "Motivational", id: "motivational" },
    { title: "Famous people", id: "famous-people" },
    { title: "Saying", id: "saying" },
    { title: "Humour", id: "humour" },
];
