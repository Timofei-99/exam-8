import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import axiosQuote from "../../axios-quotes";
import CategoryLink from "../../Components/CategoryLink/CategoryLink";
import Quote from "../../Components/Quote/Quote";
import { CATEGORY } from "../../constabs";
import "./QuotesList.css";

const QuotesList = (props) => {
    const id = props.match.params.id;
    const [quotes, setQuotes] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            if (id === undefined) {
                const response = await axiosQuote("quotes.json");
                const array = [];
                if (response.data !== null) {
                    for (const key in response.data) {
                        if (response.data.hasOwnProperty(key)) {
                            const element = response.data[key];
                            array.push({ ...element, id: key });
                        }
                    }
                    setQuotes(array);
                }
            } else {
                const response = await axiosQuote('/quotes.json?orderBy="category"&equalTo="' + id + '"');
                const array = [];
                if (response.data !== null) {
                    for (const key in response.data) {
                        if (response.data.hasOwnProperty(key)) {
                            const element = response.data[key];
                            array.push({ ...element, id: key });
                        }
                    }
                    setQuotes(array);
                }
            }
        };
        fetchData().catch(console.error);
    }, [id]);

    const list = CATEGORY.map((category) => {
        return (
            <CategoryLink key={category.id} title={category.title} id={category.id} />
        );
    });

    const quote = quotes.map((quote) => {
        return (
            <Quote
                key={quote.id}
                id={quote.id}
                author={quote.author}
                text={quote.text}
                category={quote.category}
            />
        );
    }).reverse();

    return (
        <div className="QuotesList">
            <ul>
                <li>
                    <NavLink to="/">All</NavLink>
                </li>
                {list}
            </ul>
            <div className="Quotes">{quote}</div>
        </div>
    );
};

export default QuotesList;
