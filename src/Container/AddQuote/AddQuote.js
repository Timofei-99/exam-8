import React, { useState } from "react";
import axiosQuote from "../../axios-quotes";
import { CATEGORY } from "../../constabs";
import "./AddQuote.css";

const AddQuote = (props) => {
    const [quote, setQuote] = useState({
        category: "",
        author: "",
        text: "",
    });

    const quoteDataChanged = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setQuote((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const postHandler = async (event) => {
        event.preventDefault();

        const quotes = { ...quote };

        try {
            await axiosQuote.post("/quotes.json", quotes);
        } finally {
            props.history.push("/");
        }
    };

    const option = CATEGORY.map((category) => {
        return (
            <option key={category.id} value={category.id}>
                {category.title}
            </option>
        );
    });

    return (
        <form className="AddQuote" onSubmit={postHandler}>
            <h3>Add New Quote</h3>
            <label>
                Category
                <select name="category" onChange={quoteDataChanged}>
                    <option defaultValue={quote.category}></option>
                    {option}
                </select>
            </label>
            <label>
                Author
                <input
                    type="text"
                    name="author"
                    className="Field"
                    value={quote.author}
                    onChange={quoteDataChanged}
                />
            </label>
            <label>
                Quote text
                <textarea
                    type="text"
                    name="text"
                    cols="100"
                    rows="10"
                    className="Field"
                    value={quote.text}
                    onChange={quoteDataChanged}
                />
            </label>
            <button>Save</button>
        </form>
    );
};

export default AddQuote;
