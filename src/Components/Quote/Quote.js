import React from "react";
import { NavLink } from "react-router-dom";
import axiosQuote from "../../axios-quotes";
import "./Quote.css";

const Quote = (props) => {
    const deleteQuote = async (id) => {
        try {
            await axiosQuote.delete("/quotes/" + id + ".json");
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <div className="Quote">
            <p>"{props.text}"</p>
            <p>- {props.author}</p>
            <NavLink onClick={() => deleteQuote(props.id)} to="/" className="btn">
                Delete
            </NavLink>
            <NavLink to={"/edit/" + props.id} className="btn">
                Edit
            </NavLink>
        </div>
    );
};

export default Quote;
