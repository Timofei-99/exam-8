import React, { useEffect, useState } from "react";
import axiosQuote from "../../axios-quotes";
import { CATEGORY } from "../../constabs";
import "./QuoteEdit.css";

const QuoteEdit = (props) => {
    const id = props.match.params.id;
    const [quote, setQuote] = useState({
        category: "",
        author: "",
        text: "",
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosQuote.get("/quotes/" + id + ".json");
            let categoryName;

            if (response.data.category === "star-wars") {
                categoryName = "Star Wars";
            } else if (response.data.category === "motivational") {
                categoryName = "Motivational";
            } else if (response.data.category === "famous-people") {
                categoryName = "Famous People";
            } else if (response.data.category === "saying") {
                categoryName = "Saying";
            } else if (response.data.category === "humour") {
                categoryName = "Humour";
            }

            setQuote({ ...response.data, title: categoryName });
        };
        fetchData().catch(console.error);
    }, [id]);

    const quoteDataChanged = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setQuote((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const postHandler = async (event) => {
        event.preventDefault();
        const quotes = { ...quote };
        try {
            await axiosQuote.put("/quotes/" + id + ".json", quotes);
        } finally {
            props.history.push("/");
        }
    };

    const option = CATEGORY.map((category) => {
        return (
            <option key={category.id} value={category.id}>
                {category.title}
            </option>
        );
    });

    return (
        <form className="QuoteEdit" onSubmit={postHandler}>
            <h3>Edit Quote</h3>
            <label>
                Category
                <select name="category" onChange={quoteDataChanged}>
                    <option defaultValue={quote.category} >{quote.title}</option>
                    {option}
                </select>
            </label>
            <label>
                Author
                <input
                    type="text"
                    name="author"
                    className="Field"
                    value={quote.author}
                    onChange={quoteDataChanged}
                />
            </label>
            <label>
                Quote text
                <textarea
                    type="text"
                    name="text"
                    cols="100"
                    rows="10"
                    className="Field"
                    value={quote.text}
                    onChange={quoteDataChanged}
                />
            </label>
            <button>Save</button>
        </form>
    );
};

export default QuoteEdit;
