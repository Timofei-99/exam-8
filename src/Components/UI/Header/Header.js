import React from "react";
import {NavLink} from "react-router-dom";
import "./Header.css";

const Header = () => {
    return (
        <header className="Header border">
            <div className="container">
                <div className="header-inner">
                    <p>Quotes Central</p>
                    <ul>
                        <li>
                            <NavLink
                                to="/"
                                exact
                                activeStyle={{
                                    fontWeight: "bold",
                                    color: "black",
                                    textDecoration: "none",
                                }}
                            >
                                Quotes
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                to="/add-quote"
                                activeStyle={{
                                    fontWeight: "bold",
                                    color: "black",
                                    textDecoration: "none",
                                }}
                            >
                                Submit new Quotes
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
};

export default Header;
