import React from "react";
import { NavLink } from "react-router-dom";

const CategoryLink = (props) => {
    return (
        <li>
            <NavLink to={"/quotes/" + props.id}>{props.title}</NavLink>
        </li>
    );
};

export default CategoryLink;
