import axios from "axios";

const axiosQuote = axios.create({
    baseURL: "https://quotes-a0225-default-rtdb.firebaseio.com/",
});

export default axiosQuote;
